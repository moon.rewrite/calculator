//
//  numberBtn.swift
//  Culculator
//
//  Created by 青木悠 on 2021/01/06.
//

import SnapKit
import UIKit

@IBDesignable
public class CustomBtn: UIButton {
    enum style {
        static let width: CGFloat = 80
        static let height: CGFloat = 80
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }
    
    override public var intrinsicContentSize: CGSize {
        CGSize(width: style.width, height: style.height)
    }
    
    func setupView() {
        layer.borderWidth = 2.0
        layer.cornerRadius = 25
        self.backgroundColor = UIColor.green
        self.titleLabel?.font =  UIFont.boldSystemFont(ofSize: 60)
        self.setTitleColor(UIColor.black, for: .normal)
    }
}

