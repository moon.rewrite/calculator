//
//  Model.swift
//  Calculator
//
//  Created by 青木悠 on 2021/01/14.
//  Copyright © 2021 Fap2019. All rights reserved.
//

import Foundation

public protocol ModelProtocol: AnyObject {
    /// 演算処理
    func calc(numOperator: String) ->String
    /// 番号のボタン押された時の処理
    func number(num:String)->String
    
}

class Model {
    var numOperator:String = "";
    var numberOnScreen:String = "";
    var numberOnScreen2:String = "";
    var calcflg : Bool = false;
}

extension Model: ModelProtocol {
    func calc(numOperator: String) ->String{
        switch numOperator {
        case "=":
            if(self.calcflg != true){
                return numberOnScreen
            }else{
                
                
                switch self.numOperator{
                case "+":
                    if let num1 = Double(numberOnScreen),let num2 = Double(numberOnScreen2){
                    numberOnScreen = String(num1 + num2)
                    }
                case "-":
                    if let num1 = Double(numberOnScreen),let num2 = Double(numberOnScreen2){
                    numberOnScreen = String(num1 - num2)
                    }
                case "×":
                    if let num1 = Double(numberOnScreen),let num2 = Double(numberOnScreen2){
                    numberOnScreen = String(num1 * num2)
                    }
                case "÷":
                    if(numberOnScreen2 != "0"){
                        if let num1 = Double(numberOnScreen),let num2 = Double(numberOnScreen2){
                        numberOnScreen = String(num1 / num2)
                        }
                    }
                default:
                    return numberOnScreen
                }
                numberOnScreen2 = ""
                self.calcflg=false
            }
            
        case "+":
            self.numOperator = numOperator
        case "-":
            self.numOperator = numOperator
        case "×":
            self.numOperator = numOperator
        case "÷":
            self.numOperator = numOperator
        case "C":
            numberOnScreen = ""
            numberOnScreen2 = ""
            self.numOperator = ""
            calcflg = false
        default:
            break
        }
        return numberOnScreen
        
    }
    func number(num:String) ->String{
        if(!numOperator.isEmpty){
            if (!(num == "0" && numberOnScreen2 == "0")){
                if(numberOnScreen2 == "0" && num != "0"){
                    numberOnScreen2 = num
                }else{
            numberOnScreen2 += num
                }
            }
            self.calcflg = true
            return numberOnScreen2
        }else{
            if (!(num == "0" && numberOnScreen == "0")){
                if(numberOnScreen == "0" && num != "0"){
                    numberOnScreen = num
                }else{
            numberOnScreen += num
                }
            }
              return numberOnScreen
        }
        
    }
}
