//
//  Presenter.swift
//  Calculator
//
//  Created by 青木悠 on 2021/01/14.
//  Copyright © 2021 Fap2019. All rights reserved.
//
import Foundation
protocol PresenterProtocol: class {
    /// 戻るボタンが押下されたら呼ばれるメソッド
    func update(result:String)
    
    func tapNumBtn(num:String)
    
    func tapOpeBtn(ope:String)
}

final class Presenter: PresenterProtocol {
    private weak var view: ViewProtocol!
    private var model: ModelProtocol!


    init(view: ViewProtocol, model: ModelProtocol) {
        self.view = view
        self.model = model
    }
    
    // Modelから通知があったらViewに更新を依頼
    func update(result:String) {
        view.resultOndisplay(number: result)
    }
    //Modelに番号ボタンが押されたことを通知
    func tapNumBtn(num:String) {
        update(result: model.number(num: num))
    }
    //Modelに演算子ボタンが押されたことを通知
    func tapOpeBtn(ope:String) {
        update(result: model.calc(numOperator: ope))
    }
}
