//
//  ContentView.swift
//  Calculator
//
//  Created by 青木悠 on 2021/01/11.
//

import SnapKit
import UIKit

public protocol CalculatorViewDelegate: AnyObject {
    func didTapNumberButton(number : String)
    func didTapNumOperatorButton(numOpe: String)
}

@IBDesignable
public class View: UIView {
    enum style {
        static let width: CGFloat = UIScreen.main.bounds.width
        static let height: CGFloat = 70
        
        static let topMargin: CGFloat = 60
        static let leftMargin: CGFloat = 16
        static let bottomMargin: CGFloat = 40
        
        static let lineTopMargin: CGFloat = 64
        static let lineSideMargin: CGFloat = 8
        static let lineHeight: CGFloat = 0.5
        
        static let titleFont = UIFont.systemFont(ofSize: 28)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }
    
    //表示用ラベル
    let screenText = UILabel()
    //数字のボタン
    let numberBtn0 = CustomBtn()
    let numberBtn1 = CustomBtn()
    let numberBtn2 = CustomBtn()
    let numberBtn3 = CustomBtn()
    let numberBtn4 = CustomBtn()
    let numberBtn5 = CustomBtn()
    let numberBtn6 = CustomBtn()
    let numberBtn7 = CustomBtn()
    let numberBtn8 = CustomBtn()
    let numberBtn9 = CustomBtn()
    
    //演算子
    let plusBtn = CustomBtn()
    let minusBtn = CustomBtn()
    let byBtn = CustomBtn()
    let divideBtn = CustomBtn()
    
    //イコールボタン
    let equalBtn = CustomBtn()
    
    //消去ボタン
    let deleteBtn = CustomBtn()
    
    public weak var delegate: CalculatorViewDelegate?
    
    func setupView() {
        self.backgroundColor = UIColor.gray
        
        self.addSubview(screenText)
        screenText.backgroundColor = UIColor.lightGray
        screenText.layer.borderWidth = 2.0
        screenText.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(style.topMargin)
            make.left.equalToSuperview().offset(style.leftMargin)
            make.width.equalTo(380)
            make.height.equalTo(80)
         }
        self.addSubview(deleteBtn)
        deleteBtn.setTitle("C", for: .normal)
        deleteBtn.snp.makeConstraints { make in
            make.top.equalTo(screenText.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(style.leftMargin)
         }
        self.addSubview(plusBtn)
        plusBtn.setTitle("+", for: .normal)
        plusBtn.snp.makeConstraints { make in
            make.top.equalTo(screenText.snp.bottom).offset(30)
            make.left.equalTo(deleteBtn.snp.right).offset(220)
         }
        self.addSubview(numberBtn1)
        numberBtn1.setTitle("1", for: .normal)
        numberBtn1.snp.makeConstraints { make in
            make.top.equalTo(deleteBtn.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(style.leftMargin)
         }
        self.addSubview(numberBtn2)
        numberBtn2.setTitle("2", for: .normal)
        numberBtn2.snp.makeConstraints { make in
            make.top.equalTo(deleteBtn.snp.bottom).offset(30)
            make.left.equalTo(numberBtn1.snp.right).offset(20)
         }
        self.addSubview(numberBtn3)
        numberBtn3.setTitle("3", for: .normal)
        numberBtn3.snp.makeConstraints { make in
            make.top.equalTo(deleteBtn.snp.bottom).offset(30)
            make.left.equalTo(numberBtn2.snp.right).offset(20)
         }
        self.addSubview(minusBtn)
        minusBtn.setTitle("-", for: .normal)
        minusBtn.snp.makeConstraints { make in
            make.top.equalTo(plusBtn.snp.bottom).offset(20)
            make.left.equalTo(numberBtn3.snp.right).offset(20)
         }
        self.addSubview(numberBtn4)
        numberBtn4.setTitle("4", for: .normal)
        numberBtn4.snp.makeConstraints { make in
            make.top.equalTo(numberBtn1.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(style.leftMargin)
         }
        self.addSubview(numberBtn5)
        numberBtn5.titleLabel?.adjustsFontSizeToFitWidth = true
        numberBtn5.setTitle("5", for: .normal)
        numberBtn5.snp.makeConstraints { make in
            make.top.equalTo(numberBtn1.snp.bottom).offset(20)
            make.left.equalTo(numberBtn4.snp.right).offset(20)
         }
        self.addSubview(numberBtn6)
        numberBtn6.setTitle("6", for: .normal)
        numberBtn6.snp.makeConstraints { make in
            make.top.equalTo(numberBtn1.snp.bottom).offset(20)
            make.left.equalTo(numberBtn5.snp.right).offset(20)
         }
        self.addSubview(byBtn)
        byBtn.setTitle("×", for: .normal)
        byBtn.snp.makeConstraints { make in
            make.top.equalTo(numberBtn1.snp.bottom).offset(20)
            make.left.equalTo(numberBtn6.snp.right).offset(20)
         }
        self.addSubview(numberBtn7)
        numberBtn7.setTitle("7", for: .normal)
        numberBtn7.snp.makeConstraints { make in
            make.top.equalTo(numberBtn4.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(style.leftMargin)
         }
        self.addSubview(numberBtn8)
        numberBtn8.setTitle("8", for: .normal)
        numberBtn8.snp.makeConstraints { make in
            make.top.equalTo(numberBtn4.snp.bottom).offset(20)
            make.left.equalTo(numberBtn7.snp.right).offset(20)
         }
        self.addSubview(numberBtn9)
        numberBtn9.setTitle("9", for: .normal)
        numberBtn9.snp.makeConstraints { make in
            make.top.equalTo(numberBtn4.snp.bottom).offset(20)
            make.left.equalTo(numberBtn8.snp.right).offset(20)
         }
        self.addSubview(divideBtn)
        divideBtn.setTitle("÷", for: .normal)
        divideBtn.snp.makeConstraints { make in
            make.top.equalTo(numberBtn4.snp.bottom).offset(20)
            make.left.equalTo(numberBtn9.snp.right).offset(20)
         }

        self.addSubview(numberBtn0)
        numberBtn0.setTitle("0", for: .normal)
        numberBtn0.snp.makeConstraints { make in
            make.top.equalTo(numberBtn7.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(style.leftMargin)
         }
        self.addSubview(equalBtn)
        equalBtn.setTitle("=", for: .normal)
        equalBtn.snp.makeConstraints { make in
            make.top.equalTo(numberBtn7.snp.bottom).offset(20)
            make.left.equalTo(numberBtn9.snp.right).offset(20)
         }
        
        numberBtn1.addTarget(self, action: #selector(didTapNumberBtn1), for: .touchUpInside)
        numberBtn2.addTarget(self, action: #selector(didTapNumberBtn2), for: .touchUpInside)
        numberBtn3.addTarget(self, action: #selector(didTapNumberBtn3), for: .touchUpInside)
        numberBtn4.addTarget(self, action: #selector(didTapNumberBtn4), for: .touchUpInside)
        numberBtn5.addTarget(self, action: #selector(didTapNumberBtn5), for: .touchUpInside)
        numberBtn6.addTarget(self, action: #selector(didTapNumberBtn6), for: .touchUpInside)
        numberBtn7.addTarget(self, action: #selector(didTapNumberBtn7), for: .touchUpInside)
        numberBtn8.addTarget(self, action: #selector(didTapNumberBtn8), for: .touchUpInside)
        numberBtn9.addTarget(self, action: #selector(didTapNumberBtn9), for: .touchUpInside)
        numberBtn0.addTarget(self, action: #selector(didTapNumberBtn0), for: .touchUpInside)
        plusBtn.addTarget(self, action: #selector(didTapPlusBtn), for: .touchUpInside)
        minusBtn.addTarget(self, action: #selector(didTapMinusBtn), for: .touchUpInside)
        byBtn.addTarget(self, action: #selector(didTapByBtn), for: .touchUpInside)
        divideBtn.addTarget(self, action: #selector(didTapDivideBtn), for: .touchUpInside)
        deleteBtn.addTarget(self, action: #selector(didTapDeleteBtn), for: .touchUpInside)
        equalBtn.addTarget(self, action: #selector(didTapEqualBtn), for: .touchUpInside)

    }
    @objc func didTapNumberBtn1(_ sender: Any) {
        delegate?.didTapNumberButton(number: "1")
    }
    @objc func didTapNumberBtn2(_ sender: Any) {
        delegate?.didTapNumberButton(number: "2")
    }
    @objc func didTapNumberBtn3(_ sender: Any) {
        delegate?.didTapNumberButton(number: "3")
    }
    @objc func didTapNumberBtn4(_ sender: Any) {
        delegate?.didTapNumberButton(number: "4")
    }
    @objc func didTapNumberBtn5(_ sender: Any) {
        delegate?.didTapNumberButton(number: "5")
    }
    @objc func didTapNumberBtn6(_ sender: Any) {
        delegate?.didTapNumberButton(number: "6")
    }
    @objc func didTapNumberBtn7(_ sender: Any) {
        delegate?.didTapNumberButton(number: "7")
    }
    @objc func didTapNumberBtn8(_ sender: Any) {
        delegate?.didTapNumberButton(number: "8")
    }
    @objc func didTapNumberBtn9(_ sender: Any) {
        delegate?.didTapNumberButton(number: "9")
    }
    @objc func didTapNumberBtn0(_ sender: Any) {
        delegate?.didTapNumberButton(number: "0")
    }
    
    @objc func didTapPlusBtn(_ sender: Any) {
        delegate?.didTapNumOperatorButton(numOpe: "+")
        }
    
    @objc func didTapMinusBtn(_ sender: Any) {
        delegate?.didTapNumOperatorButton(numOpe: "-")
        }
    
    @objc func didTapByBtn(_ sender: Any) {
        delegate?.didTapNumOperatorButton(numOpe: "×")
        }
    
    @objc func didTapDivideBtn(_ sender: Any) {
        delegate?.didTapNumOperatorButton(numOpe: "÷")
        }
    
    @objc func didTapDeleteBtn(_ sender: Any) {
        delegate?.didTapNumOperatorButton(numOpe: "C")
        }
    
    @objc func didTapEqualBtn(_ sender: Any) {
        delegate?.didTapNumOperatorButton(numOpe: "=")
        }
    

}


