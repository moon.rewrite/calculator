//
//  ViewController.swift
//  Calculator
//
//  Created by 月見里浩太 on 2021/01/11.
//  Copyright © 2021 Fap2019. All rights reserved.
//

import UIKit
import SnapKit

public protocol ViewProtocol: AnyObject {
    /// 計算結果表示
    func resultOndisplay(number: String)
}
class ViewController: UIViewController {
    @IBOutlet weak var calculatorView : View!
    var presenter: Presenter!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        calculatorView.delegate = self
        presenter = Presenter(view: self,model: Model())
        
    }
}
    
extension ViewController: ViewProtocol {
    func resultOndisplay(number: String) {
        calculatorView.screenText.text = String(number)
    }
    }
        

    
extension ViewController: CalculatorViewDelegate {
    func didTapNumOperatorButton(numOpe: String) {
        presenter.tapOpeBtn(ope: numOpe)
    }
    
    func didTapNumberButton(number: String) {
        presenter.tapNumBtn(num: number)

    }
    }

